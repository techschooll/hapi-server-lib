const UserRoles = {
  admin: "ADM",
  author: "AUT",
  user: "USR" 
}

const localExports = {
  UserRoles
}

module.exports = localExports