const _ = require('lodash')
const Jwt = require('jsonwebtoken')
const Boom = require('boom')

class Helper {
  constructor(requestContext) {
    this.requestContext = requestContext
  }

  async authenticateRequest(jwt, request, config, validRoles = []) {
    try {
      if (!jwt) {
        throw Boom.badRequest("jwt is required")
      }
      await Jwt.verify(jwt, _.get(config, `jwt.secret`), function(err, decoded) {
        let rolesInJwt = _.get(decoded, `data.roles`)
        if(_.isEmpty(_.intersection(validRoles, rolesInJwt))) {
          throw Boom.unauthorized("unauthorized")
        }
        request.headers["x-internal-credentials"] = JSON.stringify(decoded)
        return true
      })
    } catch (e) {
      throw e
    }
  }
}

module.exports = Helper