const axios = require('axios');


class DataAdapter {
  constructor(requestContext) {
    this.requestContext = requestContext
  }

  async get(url) {
    try {
      let response = await axios.get(url)
      return response
    }
    catch(e) {
      throw e
    }
  }

  async post(url, payload) {
    try {
      let response = await axios.post(url, payload)
      return response
    }
    catch(e) {
      throw e
    }
  }

  async put(url, payload) {
    try {
      let response = await axios.put(url, payload)
      return response
    }
    catch(e) {
      throw e
    }
  }

  async delete(url) {
    try {
      let response = await axios.delete(url)
      return response
    }
    catch(e) {
      throw e
    }
  }
}

module.exports = DataAdapter