const _ = require('lodash')
const hapi = require('hapi')
const h2o2 = require('h2o2')
const hapiSwaggered = require('hapi-swaggered')
const hapiSwaggeredUi = require('hapi-swaggered-ui')

class AppLoader {
  constructor(config, dependencies) {
    this.config = config
    this.dependencies = dependencies
    this.server = hapi.server({
      port: _.get(config, `server.port`) || 3000,
      host: _.get(config, `server.host`) || 'localhost'
    });
  }

  async registerRoutes() {
    //overridden in services
  }

  async registerDependencies(config) {
    //can be overridden in services
    await this.server.register(h2o2)
    await this.server.register([
      require('inert'),
      require('vision'), {
        plugin: hapiSwaggered,
        options: {
          info: {
            title: 'Service Documentation',
            version: "v1",
          },
          endpoint: "/swagger"
        }
      }, {
        plugin: hapiSwaggeredUi,
        options: {
          title: "swagger UI",
          path: "/documentation",
          swaggerOptions: {
            validatorUrl: false
          }
        }
      }
    ]);

  }


  async startServer() {
    await this.registerDependencies(this.config)
    await this.registerRoutes(this.config)
    console.log(_.get(this, `config.service`) + " has started in port " + _.get(this, `config.server.port`))
    await this.server.start()
    process.on('unhandledRejection', (err) => {
      process.exit(1);
    })
  }
}

module.exports = AppLoader;